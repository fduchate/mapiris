#  ![mapiris](/mapiris/static/img/favicon.png?raw=true "Logo mapiris") &nbsp;&nbsp;&nbsp; mapiris

Cette application permet de visualiser les [IRIS](https://www.insee.fr/fr/metadonnees/definition/c1523) (zones administratives définies par l'INSEE, un peu similaires aux quartiers, environ 50000 IRIS sur le terrtitoire français) et les indicateurs qui les décrivent (e.g., nombre de boulangeries, nombre et type d'établissements scolaires, pourcentage d'habitant.e.s selon les catégories socio-professionnelles).

L'outil *mapiris* permet de chercher les IRIS par code, par nom (d'IRIS ou de commune) et d'afficher les IRIS sur une zone géographique donnée.

<img src="/mapiris/static/img/screenshot-mapiris-carte.jpg?raw=true" alt="Capture mapiris carte" width="100%">

Les indicateurs sur un IRIS peuvent être visualisés sur une page détaillée.

<img src="/mapiris/static/img/screenshot-mapiris-details.jpg?raw=true" alt="Capture mapiris details iris" width="100%">

## Pré-requis

- Python, version >=3
- [MongoDB](https://www.mongodb.com/), version >=4, pour lequel il faudra importer la base de données des IRIS (cf installation).

## Installation

Pour installer *mapiris*, se postionner à la racine du répertoire `mapiris/` et saisir dans un terminal :

```
python3 -m pip install -e . -r requirements.txt
```

Cette commande installe les dépendances, dont [mongiris](https://gitlab.liris.cnrs.fr/fduchate/mongiris) qui permet l'interrogation d'une base de données sous MongoDB contenant les IRIS. **Attention, long téléchargement (700 Mo)**.

Il est donc nécessaire de créér la base de données avec la collection d'IRIS. Pour cela, exécuter la commande (depuis le répertoire des exécutables de MongoDB si besoin) :

```
./mongorestore --archive=/path/to/dump-iris.bin
```

où `/path/to/` représente le chemin vers le fichier dump de la collection des IRIS (fourni de base avec le package mongiris dans `mongiris/data/dump-iris.bin`)

## Lancement de l'interface

Pour lancer *mapiris*, taper dans un terminal :

```
cd mapiris/
python3 main.py
```

Après quelques informations, le terminal affiche l'URL permettant de tester *mapiris* : `http://localhost:8081/`

## Crédits

Source de données : [INSEE](https://www.insee.fr/)

Contributeurs : laboratoire [LIRIS](https://liris.cnrs.fr/), laboratoire [CMW](https://www.centre-max-weber.fr/) et Labex [Intelligence des Mondes Urbains (IMU)](http://imu.universite-lyon.fr/projet/hil)

