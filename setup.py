#!/usr/bin/env python
from setuptools import setup
import shutil

def delete_dir(directories):  # delete directories (ignore errors such as read-only files)
    for directory in directories:
        shutil.rmtree(directory, ignore_errors=True)


# delete build/config directories because egg-info only updates config files for new versions
delete_dir(['./mapiris.egg-info/', './build/', './dist/'])

readme = open("README.md").read()

setup(long_description=readme)

