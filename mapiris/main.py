#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
#   main.py: runs the Flask server (http://flask.pocoo.org/) using routes
# =============================================================================


from flask import Flask, flash, render_template, request, send_from_directory, url_for, jsonify
import webbrowser
import os, json
import model

app = Flask(__name__)
url = 'http://127.0.0.1:8081'


@app.route('/', defaults={'page': None})
def index(page):
    if not model.db.connection_status:  # if no connection, display a flashing message
        flash("Could not connect to the MongoDB database ! Check the connection.", "danger")
    return render_template('index.html')


@app.route('/details-iris.html', methods=["GET"])
def get_details_iris():
    code_iris = request.args['code_iris']
    iris = model.get_iris_from_code(code_iris)
    dict_code_label = model.parse_json_to_dict(model.json_iris_indicator_code_to_label)
    if iris is None:
        flash("No corresponding iris for code " + code_iris + ".", "warning")
    return render_template('details-iris.html', iris=iris, dict_code_label=dict_code_label)


@app.route('/getIrisPolygon', methods=["GET"])
def get_iris_for_polygon():
    lat1 = float(request.args['lat1'])
    lng1 = float(request.args['lng1'])
    lat2 = float(request.args['lat2'])
    lng2 = float(request.args['lng2'])
    iris = model.get_iris_for_polygon(lat1, lng1, lat2, lng2)
    if iris is None or len(iris) == 0:
        flash("No iris found in the area.", "warning")
    else:
        flash(str(len(iris)) + " iris found in the area.", "success")
    return json.dumps({'status': 'OK', 'geojson': iris})


@app.route('/countIrisPolygon', methods=["GET"])
def count_iris_for_polygon():
    lat1 = float(request.args['lat1'])
    lng1 = float(request.args['lng1'])
    lat2 = float(request.args['lat2'])
    lng2 = float(request.args['lng2'])
    nb_iris = model.count_iris_for_polygon(lat1, lng1, lat2, lng2)
    return json.dumps({'status': 'OK', 'nbIris': nb_iris})


@app.route('/searchCode', methods=["GET"])
def get_iris_from_code():
    code_iris = request.args['codeIris']
    iris = model.get_iris_from_code(code_iris)
    if iris is None:
        flash("No corresponding iris for code " + code_iris + ".", "warning")
    else:
        flash("Found iris " + code_iris + ".", "success")
    return json.dumps({'status': 'OK', 'geojson': iris})


@app.route('/searchName', methods=["GET"])
def get_iris_from_name():
    query = request.args['querySearch']
    iris = model.get_iris_from_name(query)
    if iris is None or len(iris) == 0:
        flash("No corresponding iris for query " + query + ".", "warning")
    else:
        flash(str(len(iris)) + " iris found for query " + query + ".", "success")
    return json.dumps({'status': 'OK', 'geojson': iris})


@app.route('/favicon.ico')
@app.route('/<page>/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.png', mimetype='image/favicon.png')


if __name__ == '__main__':
    webbrowser.open_new(url)
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0  # do not cache files, especially static files such as JS
    app.secret_key = 's3k_5Et#fL45k_#ranD0m-(StuF7)'
    app.run(port=8081)  # debug = True

