#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
#   model.py: methods for getting data from MongoDB (using mongiris) and transforming it
# =============================================================================

from mongiris.api import Mongiris
import re
import json

# connection to the IRIS collection in MongoDB
db = Mongiris()
iris_collection = db.collection_neighbourhoods
json_iris_indicator_code_to_label = 'static/data/dictionnaire-indicateurs.json'


def get_iris_for_polygon(lat1, lng1, lat2, lng2):
    #polygon = db.convert_geojson_box_to_polygon(lng1, lat1, lng2, lat2)
    polygon = Mongiris.convert_geojson_box_to_polygon(lng1, lat1, lng2, lat2)
    #iris = db.geo_within(iris_collection, polygon)
    iris = db.intersect(iris_collection, polygon)
    return iris


def count_iris_for_polygon(lat1, lng1, lat2, lng2):
    polygon = db.convert_geojson_box_to_polygon(lng1, lat1, lng2, lat2)
    iris = db.geo_within(iris_collection, polygon)
    if iris is None:
        return 0
    return len(iris)


def get_iris_from_code(code_iris):
    iris = db.get_neighbourhood_from_code(code_iris)
    return iris


def get_iris_from_name(name):
    # the query string (name) is searched in both the name of the iris and the name of the city
    regx = re.compile(name, re.IGNORECASE)
    query_clause = {"$or": [{"properties.NOM_IRIS": {"$regex": regx}}, {"properties.NOM_COM": {"$regex": regx}}]}
    iris = db.find_documents(iris_collection, query_clause)
    return iris


def parse_json_to_dict(json_file_path):
    with open(json_file_path) as data_file:
        data = json.load(data_file)
        data_file.close()
        return data


