/*
** Non-cartographic and ajax-related functions
*/

// parameters
let styleDisplayedIris = {style : {color: "#000000", fillColor: "#000000", fillOpacity: 0}};


function refreshMessages() {
    $("#zoneMessages").load(location.href + " #zoneMessages");  // reload #zoneMessages to display messages
}


/**
  * Call the AJAX request to get an iris from a given code
  * @param{object} e : an EventObject describing the event
  * @return a GeoJSON object containing a field 'status' and a field 'geojson' (list of iris)
**/
function getIrisFromCode(e) {
	console.log("Executing getIrisFromCode()...");
	$.ajax({
		type: "GET",
		url: "/searchCode",
		data: {
			'codeIris': $("#inputCode").val()
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			jsonResult = JSON.parse(result);
            irisLayer = addLayerFromGeoJSON(jsonResult['geojson'], eventsIRIS, styleDisplayedIris, "searchCode");
            refreshMessages();
		},
		error: function(result, textStatus, errorThrown) {
            console.log(errorThrown);
		}
	});
}

/**
  * Call the AJAX request to get an iris from a given search query (i.e., name of iris or city)
  * @param{object} e : an EventObject describing the event
  * @return a GeoJSON object containing a field 'status' and a field 'geojson' (list of iris)
**/
function getIrisFromName(e) {
	console.log("Executing getIrisFromName()...");
	$.ajax({
		type: "GET",
		url: "/searchName",
		data: {
			'querySearch': $("#inputName").val()
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			jsonResult = JSON.parse(result);
            irisLayer = addLayerFromGeoJSON(jsonResult['geojson'], eventsIRIS, styleDisplayedIris, "searchName");
            refreshMessages();
		},
		error: function(result, textStatus, errorThrown) {
            console.log(errorThrown);
		}
	});
}


/**
  * Call the AJAX request to retrieve iris in the bounded area
  * @param{array} areaBounds : a pair of coordinates (latitude, longitude) delimiting a rectangular area
  * @return a GeoJSON object containing a field 'status' and a field 'geojson' (list of iris)
**/
function getIrisForBounds(areaBounds) {
	console.log("Executing getIrisForBounds()...");
	$.ajax({
		type: "GET",
		url: "/getIrisPolygon",
		data: {
			'lat1': areaBounds["_southWest"]["lat"],  // latitude southwest point
			'lng1': areaBounds["_southWest"]["lng"],  // longitude southwest point
			'lat2': areaBounds["_northEast"]["lat"],  // latitude northeast point
			'lng2': areaBounds["_northEast"]["lng"],  // longitude northeast point
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			jsonResult = JSON.parse(result);
            irisLayer = addLayerFromGeoJSON(jsonResult['geojson'], eventsIRIS, styleDisplayedIris, "searchBounds");
            refreshMessages();
		},
		error: function(result, textStatus, errorThrown) {
			/*
			$("#loading").hide();
			if(document.getElementById("danger") == null) {
                $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La recommandation a rencontré un problème, veuillez recommencer...</div>");
            }
            */
            console.log(errorThrown);
		}
	});
}

/**
  * Call the AJAX request to count the number of iris in the bounded area
  * @param{array} areaBounds : a pair of coordinates (latitude, longitude) delimiting a rectangular area
  * @param{object} callback : a method which is executed
  * @return a GeoJSON object containing a field 'status' and a field 'nbIris'
**/
function countIrisForBounds(areaBounds, callback) {
	console.log("Executing countIrisForBounds()...");
	$.ajax({
		type: "GET",
		url: "/countIrisPolygon",
		data: {
			'lat1': areaBounds["_southWest"]["lat"],  // latitude southwest point
			'lng1': areaBounds["_southWest"]["lng"],  // longitude southwest point
			'lat2': areaBounds["_northEast"]["lat"],  // latitude northeast point
			'lng2': areaBounds["_northEast"]["lng"],  // longitude northeast point
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			jsonResult = JSON.parse(result);
			nbIris = jsonResult['nbIris'];
			console.log("nb iris in area = " + nbIris);
            return jsonResult['nbIris'];
		},
		error: function(result, textStatus, errorThrown) {
            console.log(errorThrown);
		}
	});
}


