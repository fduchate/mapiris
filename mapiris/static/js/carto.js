/*
* Functions for Leaflet.js cartographic library (init Leaflet map, load a GeoJSON file as a layer, etc.)
*/

// parameters
const ZOOM_SNAP = 0.5;  // step value for which the zoom increases/decreases (0.5 for finer tuning, else 1.0)
const DEFAULT_ZOOM_LEVEL_MIN_FOR_DISPLAY = 12; // zoom level above which iris are displayed (default 12)
const NB_IRIS_MAX_FOR_DISPLAY = 250; // max number of iris iris to enable display (default 250) - not used, the costly
// operation is to extract iris (>500) from database, not the display or json parsing

// global variables
let map = null; // leaflet map
let baseLayers = null; // array of basic layers
let overlayLayers = null; // array of overlaying layers
let osmLayer = null; // openstreetmap basic layer
let irisLayer = null; // layer of displayed IRIS


/**
 * Initialize the map
 */
function initialize() {
    // creation of the map and general settings : option zoom snap degree, centering, zoom level
    map = L.map("map-id", {zoomSnap: ZOOM_SNAP}).setView([46.895143, 2.397461], 6);
    // creation of an OSM layer
    osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        "attribution": 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        "maxZoom": 18,
        "detectRetina": false,
        "minZoom": 1,
        "noWrap": false,
        "subdomains": "abc"
    }).addTo(map);
    // legend for layers
    baseLayers = {'OpenStreetMap' : osmLayer};
    // add the zoom change event
    map.addEventListener("zoomend", zoomendEvent);
}


/*
** Event for zoom changes : updates a label and if zoom enabled and above min zoom level, display iris
*/
function zoomendEvent() {
    zoomLevel = map.getZoom();
    document.getElementById("spanZoomLevel").innerHTML = zoomLevel;
    isZoomDisabled = $("#inputZoomLevel").prop("disabled");
    if(!isZoomDisabled) {  // display after zoomend is enabled
        let minZoomLevel = $("#inputZoomLevel").val();
        if(zoomLevel >= minZoomLevel) { // display iris on the selected zone
            bounds = map.getBounds();
            iris = getIrisForBounds(bounds);
        }
    }
}

/*
** Method for deleting a layer (e.g., all iris in irisLayer)
*/
function removeLayer() {
    map.removeLayer(irisLayer);
    irisLayer = null;
    $("#zoneMessages").html("");
}

/**
 * Reset the style of all highlighted layer elements
 */
function resetHighlightAll() {
    if(irisLayer != null) {
        console.log("ici");
        $.each(irisLayer["layers"], function(key, value) {
            console.log(irisLayer["layers"] + " : " + key + " ; " + value);
            irisLayer["layers"].resetStyle(key);
        });
	}
}


/**
 * Add IRIS layer from GeoJSON data
 * @param {geojson} geojson .
 * @param {event} events .
 * @param {object} style .
 * @param {string} typeMethod . (specify the method which caused this display, ex "searchBounds", "searchCode", "searchName")
 * @param {function} callback .
 */
function addLayerFromGeoJSON(geojson, events, style, typeMethod){
    if(irisLayer != null) { // if already displayed iris, remove them
        removeLayer(irisLayer);
    }
    if(geojson != null) {
        irisLayer = new L.geoJSON(geojson, {onEachFeature: events});
        irisLayer.setStyle(style);
        irisLayer.addTo(map);
        if(typeMethod != "searchBounds") // if searchBounds (zoom), fitBounds() will decrease the zoom, thus reloading searchBounds...
            map.fitBounds(irisLayer.getBounds()); // zoom on the displayed iris
    }
    // todo add markers ?
    return irisLayer;
}

/**
 * Add tooltip, mouseover, mouseout and click events to the (features of) IRIS layer
 * @param {number} feature .
 * @param {layer} layer .
 */
function eventsIRIS(feature, layer) {
	layer.on({
		//mouseover: highlightFeature,
		//mouseout: resetHighlight,
		//click: clickProperties
	});
    // set a popup about each iris
	messageTooltip = '<div>CODE IRIS : ' + feature.properties.CODE_IRIS + '<br/>IRIS : ' + feature.properties.NOM_IRIS +
	'<br/>COMMUNE : ' + feature.properties.NOM_COM + '<br/><br><a href="details-iris.html?code_iris=' +
	feature.properties.CODE_IRIS + '" target="_blank">Plus de détails</a></div>';
    layer.bindPopup(messageTooltip); // adding the popup
}

function eventValidateButton(event) {
    // a generic method to allow the validation of input fields using "enter key"
    if(event.keyCode == 13)  // enter key has been pressed and released, back to parent and click on the <button>
        event.target.parentNode.querySelector("button").click();
}



// updating values of printed parameters
$("#inputZoomLevel").val(DEFAULT_ZOOM_LEVEL_MIN_FOR_DISPLAY);

// linking elements (buttons, body) to events
$("#boutonRechercherCode").click(getIrisFromCode);
$("#inputCode").keyup(eventValidateButton);
$("#boutonRechercherNom").click(getIrisFromName);
$("#inputName").keyup(eventValidateButton);
$("#boutonEffacer").click(removeLayer);
$(document).ready(initialize); // when the page is loaded, initialize the map

$("#boutonEnableDisableZoom").click(function() {
    if($("#inputZoomLevel").prop("disabled")) {  // enable the zoom input
        $("#boutonEnableDisableZoom > img").attr("src", "./static/css/open-iconic-master/svg/lock-unlocked.svg");
        $("#inputZoomLevel").removeAttr("disabled"); // prop({disabled: false});
    }
    else {  // disable the zoom input
        $("#boutonEnableDisableZoom > img").attr("src", "./static/css/open-iconic-master/svg/lock-locked.svg");
        $("#inputZoomLevel").prop("disabled", true);
    }
})

